/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package hig;

import hig.domain.Person;
import hig.domain.RoleEnum;
import hig.repository.PersonRepository;
import hig.service.person.PersonMapper;
import hig.service.person.PersonService;
import hig.service.person.PersonSimpleDTO;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author 22gupe01
 */
@ExtendWith(MockitoExtension.class)
public class Feature_3_Test_Service {
    
    @Mock
    PersonRepository mockRepository;
    
    @Mock
    PersonMapper mockMapper;
    
    @InjectMocks
    PersonService objectUnderTest;
    
    public Feature_3_Test_Service() {   
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }
    
    @Test
    public void emptyDbMakesFirstCreateRequestAdminTestWrongRole() {
        Mockito.when(mockRepository.count()).thenReturn((long) 0);
        Mockito.when(mockRepository.countByRole(RoleEnum.ADMIN)).thenReturn((long) 0);
        
        Person person = new Person("test","test", 12, "test", "test", RoleEnum.USER);
        
        assertThrows(IllegalArgumentException.class, () -> objectUnderTest.create(person));
    }
    @Test
    public void emptyDbFirstUserAdmin() {
        Person person = new Person("firstName", "lastName", 11  , "username", "password", RoleEnum.ADMIN);
        Mockito.when(mockRepository.count()).thenReturn((long) 0);
        Mockito.when(mockRepository.countByRole(RoleEnum.ADMIN)).thenReturn((long  ) 0);
        
        Mockito.when(mockMapper.toSimpleDto(mockRepository.save(person))).thenReturn(new PersonSimpleDTO("firstName lastName", 11));
        
        assertDoesNotThrow(() -> objectUnderTest.create(person));
    }
    @Test
    public void notEmptyDbCreateUser() {
        Person person = new Person("firstName", "lastName", 11  , "username", "password", RoleEnum.USER);
        Mockito.when(mockRepository.count()).thenReturn((long) 1);
        Mockito.when(mockRepository.countByRole(RoleEnum.ADMIN)).thenReturn((long) 1);
        
        Mockito.when(mockMapper.toSimpleDto(mockRepository.save(person))).thenReturn(new PersonSimpleDTO("firstName lastName", 11));
        
        assertDoesNotThrow(() -> objectUnderTest.create(person));
    }
    
}
