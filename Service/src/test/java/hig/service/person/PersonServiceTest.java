/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package hig.service.person;

import hig.domain.Person;
import hig.repository.PersonRepository;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author Ole Granberg
 */
@ExtendWith(MockitoExtension.class)
public class PersonServiceTest {
    
    @Mock
    PersonMapper mockMapper;
    
    @Mock
    PersonRepository mockRepo;
    
    @InjectMocks
    PersonService objectUnderTest;
    
    public PersonServiceTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of getAllPeople method, of class PersonService.
     */
    @Test
    public void testGetAllPeople() {
        System.out.println("getAllPeople");
    }

    /**
     * Test of create method, of class PersonService.
     */
    @Test
    public void testCreate() {
        System.out.println("create");

    }

    /**
     * Test of getPerson method, of class PersonService.
     */
    @Test
    public void testGetPerson() {
        System.out.println("getPerson");

    }

    /**
     * Test of getPersonByUsername method, of class PersonService.
     */
    @Test
    public void testGetPersonByUsername() {
        System.out.println("getPersonByUsername");

    }

    /**
     * Test of deletePersonByUsername method, of class PersonService.
     */
    @Test
    public void testDeletePersonByUsernameSuccess() {
        System.out.println("deletePersonByUsername");
        
        String username = "Test";
        String expResult = "Person with username 'Test' deleted successfully.";
        
        Mockito.when(mockRepo.deleteByUsername(username)).thenReturn(1L);
        
        assertEquals(expResult, objectUnderTest.deletePersonByUsername(username));
    }
    
    /**
     * Test of deletePersonByUsername method, of class PersonService.
     */
    @Test
    public void testDeletePersonByUsernameFail() {
        System.out.println("deletePersonByUsername");
        
        String username = "Test";
        String expResult = "Person with username 'Test' not found.";
        
        Mockito.when(mockRepo.deleteByUsername(username)).thenReturn(0L);
        
        assertEquals(expResult, objectUnderTest.deletePersonByUsername(username));
    }
    
}
