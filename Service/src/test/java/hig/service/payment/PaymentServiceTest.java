/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package hig.service.payment;

import hig.domain.Payment;
import hig.domain.Person;
import hig.domain.RoleEnum;
import hig.repository.PaymentRepository;
import hig.repository.PersonRepository;
import hig.service.date.DateProviderAdapter;
import hig.service.person.PersonDTO;
import java.time.LocalDate;
import java.time.Month;
import java.util.Optional;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author 21axed01
 */
@ExtendWith(MockitoExtension.class)
public class PaymentServiceTest {

    @Mock
    PaymentRepository repository;

    @Mock
    PersonRepository personRepository;

    @Mock
    PaymentMapper mapper;

    @Mock
    DateProviderAdapter adapter;

    @InjectMocks
    PaymentService objectUnderTest;

    private long id = 1L;
    private float pay = 50;
    private final String firstName = "Axel";
    private final String lastName = "Persson";
    private final String username = "Test";
    private final String password = "123";
    private final int age = 55;
    private RoleEnum role = RoleEnum.ADMIN;
    private Person testPerson;
    private PersonDTO testPersonDTO;

    private final LocalDate currentDate1 = LocalDate.of(2024, Month.MAY, 6);
    private final LocalDate currentDate2 = LocalDate.of(2024, Month.MAY, 26);
    private final LocalDate inputDate1 = LocalDate.of(2024, Month.MAY, 6);
    private final LocalDate inputDate2 = LocalDate.of(2024, Month.APRIL, 24);
    private final LocalDate currentMonthPayday = LocalDate.of(2024, Month.MAY, 25);
    private final LocalDate previousMonthPayday = LocalDate.of(2024, Month.APRIL, 25);

    public PaymentServiceTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
        testPerson = new Person(firstName, lastName, age, username, password, role);
        testPersonDTO = new PersonDTO(testPerson.getFirstName(), testPerson.getLastName());
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void successfulCreateWithDateTest() {
        System.out.println("hig.service.payment.PaymentServiceTest.successfulCreateWithDateTest()");
        Mockito.when(personRepository.findById(testPerson.getId())).thenReturn(Optional.ofNullable(testPerson));
        Mockito.when(adapter.getCurrentDate()).thenReturn(currentDate1);
        Mockito.when(adapter.getPreviousMonthPayDayDate()).thenReturn(previousMonthPayday);
        Mockito.when(adapter.getCurrentMonthPayDayDate()).thenReturn(currentMonthPayday);
        Mockito.when(adapter.parse(inputDate1.toString())).thenReturn(inputDate1);
        Mockito.when(mapper.toDTO(repository.save(new Payment(testPerson, pay, inputDate1))))
                .thenReturn(new PaymentDTO(testPersonDTO, inputDate1));

        assertDoesNotThrow(() -> objectUnderTest.create(testPerson.getId(), pay, inputDate1.toString()));
    }

    @Test
    public void unsuccessfulCreateWithDateBeforePreviousMonthPaydayTest() {
        System.out.println("hig.service.payment.PaymentServiceTest.unsuccessfulCreateWithDateBeforePreviousMonthPaydayTest()");
        Mockito.when(personRepository.findById(testPerson.getId())).thenReturn(Optional.ofNullable(testPerson));
        Mockito.when(adapter.getCurrentDate()).thenReturn(currentDate1);
        Mockito.when(adapter.getPreviousMonthPayDayDate()).thenReturn(previousMonthPayday);
        Mockito.when(adapter.getCurrentMonthPayDayDate()).thenReturn(currentMonthPayday);
        Mockito.when(adapter.parse(inputDate2.toString())).thenReturn(inputDate2);

        assertThrows(IllegalArgumentException.class, () -> objectUnderTest.create(testPerson.getId(), pay, inputDate2.toString()));
    }

    @Test
    public void unsuccessfulCreateWithDateAfterCurrentMonthPaydayTest() {
        System.out.println("hig.service.payment.PaymentServiceTest.unsuccessfulCreateWithDateAfterCurrentMonthPaydayTest()");
        Mockito.when(personRepository.findById(testPerson.getId())).thenReturn(Optional.ofNullable(testPerson));
        Mockito.when(adapter.getCurrentDate()).thenReturn(currentDate2);
        Mockito.when(adapter.getPreviousMonthPayDayDate()).thenReturn(previousMonthPayday);
        Mockito.when(adapter.getCurrentMonthPayDayDate()).thenReturn(currentMonthPayday);
        Mockito.when(adapter.parse(inputDate1.toString())).thenReturn(inputDate1);

        assertThrows(IllegalArgumentException.class, () -> objectUnderTest.create(testPerson.getId(), pay, inputDate1.toString()));
    }

}
