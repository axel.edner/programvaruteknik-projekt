/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package hig.service.cleaning;

import hig.repository.CleaningRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

/**
 *
 * @author 21axed01
 */
public class CleaningServiceTest {
    
    @Mock
    CleaningMapper mockMapper;
    
    @Mock
    CleaningRepository mockRepository;
    
    @InjectMocks
    CleaningService objectUnderTest;
    
    private long id = 1;
    private long days = 1;
    
    public CleaningServiceTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }
    
    @Test
    public void getByRoomIdForCurrentDayTest() {
        System.out.println("getByRoomIdForCurrentDay");
    }
    
    @Test
    public void getByRoomIdAndDaysTest() {
        System.out.println("getByRoomIdAndDays");
    }

}
