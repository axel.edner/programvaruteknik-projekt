/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package hig.service.room;

import hig.domain.Room;
import hig.repository.CleaningRepository;
import hig.repository.RoomRepository;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author 22gupe01
 */
@ExtendWith(MockitoExtension.class)
public class RoomServiceTest {
    
    @Mock
    RoomRepository roomRepo;
    @Mock
    CleaningRepository cleaningRep;
    @Mock
    RoomMapper mapperMock;
    @InjectMocks
    RoomService objectUnderTest;
    
    
    
    public RoomServiceTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of create method, of class RoomService.
     */
    @Test
    public void testCreate() {
        System.out.println("create");
        
    }

    /**
     * Test of findAll method, of class RoomService.
     */
    @Test
    public void testFindAll() {
        
    }

    /**
     * Test of delete method, of class RoomService.
     */
    @Test
    public void testDeleteWithExistingCleanings() {
        System.out.println("delete with existing cleanings");
        long id = 3;
        String expResult = "Room has registred cleanings, can't remove";
        Mockito.when(cleaningRep.countByWhereId(id)).thenReturn(3L);
        
        assertEquals(expResult, objectUnderTest.delete(id));
    }
    @Test
    public void testDeleteWithoutExistingCleanings() {
        System.out.println("delete without existing cleanings");
        long id = 3;
        String expResult = "Room with roomnumber '" + id + "' deleted successfully.";
        Mockito.when(cleaningRep.countByWhereId(id)).thenReturn(0L);
        Mockito.when(roomRepo.deleteRoomById(id)).thenReturn(1L);
        
        assertEquals(expResult, objectUnderTest.delete(id));
    }
    @Test
    public void testDeleteWithoutExistingCleaningsAndInvalidRoomId() {
        System.out.println("delete without existing cleanings & invalid Room Id");
        long id = 3;
        String expResult = "Room with roomnumber '" + id + "' not found.";
        Mockito.when(cleaningRep.countByWhereId(id)).thenReturn(0L);
        Mockito.when(roomRepo.deleteRoomById(id)).thenReturn(0L);
        
        assertEquals(expResult, objectUnderTest.delete(id));
    }
    
}
