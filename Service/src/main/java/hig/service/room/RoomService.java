package hig.service.room;

import hig.domain.Room;
import hig.repository.CleaningRepository;
import hig.repository.RoomRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author thomas
 */
@Service
public class RoomService {
    
    private final RoomRepository roomRepository;
    private final CleaningRepository cleaningRepository;
    private final RoomMapper mapper;

    @Autowired
    public RoomService(RoomRepository repository, RoomMapper mapper, CleaningRepository cleaningRepository) {
        this.roomRepository = repository;
        this.cleaningRepository = cleaningRepository;
        this.mapper = mapper;
    }
    
    public Room create(Room room) {
        return roomRepository.save(room);
    }

    public List<RoomDTO> findAll() {
        return mapper.toDtoList(roomRepository.findAll());
    }
    public String delete(Long id) {
        
        long cleaningOccasions = cleaningRepository.countByWhereId(id);

        if (cleaningOccasions == 0) {
            long affectedRows = roomRepository.deleteRoomById(id);

            if (affectedRows >= 1) {
                return "Room with roomnumber '" + id + "' deleted successfully.";
            } else {
                return "Room with roomnumber '" + id + "' not found.";
            }
        }
        
        else
        {
            return "Room has registred cleanings, can't remove";
        }
        
        
    }
}
