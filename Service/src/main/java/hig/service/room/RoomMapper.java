package hig.service.room;

import hig.domain.Room;
import java.util.List;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

/**
 *
 * @author thomas
 */
@Component
@Mapper(componentModel = "spring")
public interface RoomMapper {

    RoomDTO toDto(Room room);

    List<RoomDTO> toDtoList(List<Room> room);
}
