/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package hig.service.date;

import java.time.LocalDate;
import org.springframework.stereotype.Component;

/**
 *
 * @author 21axed01
 */
@Component
public interface DateProvider {

    public LocalDate getCurrentDate(); 
    
    public LocalDate getPreviousMonthPayDayDate();
    
    public LocalDate getCurrentMonthPayDayDate();
    
    public LocalDate parse(String dateString);
    
}
