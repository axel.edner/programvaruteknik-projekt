/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hig.service.date;

import hig.domain.Payment;
import java.time.LocalDate;
import org.springframework.stereotype.Component;

/**
 *
 * @author 21axed01
 */
@Component
public class DateProviderAdapter implements DateProvider {

    @Override
    public LocalDate getCurrentDate() {
        return LocalDate.now();
    }

    @Override
    public LocalDate getPreviousMonthPayDayDate() {
        return getCurrentDate().minusMonths(1).withDayOfMonth(Payment.PAY_DAY);
    }

    @Override
    public LocalDate getCurrentMonthPayDayDate() {
        return getCurrentDate().withDayOfMonth(Payment.PAY_DAY);
    }
    
    @Override
    public LocalDate parse(String dateString) {
        return LocalDate.parse(dateString);
    }

}
