package hig.service.person;

import hig.domain.Person;
import hig.domain.RoleEnum;
import hig.repository.*;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author thomas
 */
@Service
public class PersonService {

    private final PersonRepository repository;
    private final PersonMapper mapper;

    @Autowired
    public PersonService(
            PersonRepository repository,
            PersonMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    public List<PersonSimpleDTO> getAllPeople() {
        return mapper.toSimpleDtoList(repository.findAll());
    }

    public PersonSimpleDTO create(Person person) {
        long userCount = repository.count();
        long adminCount = repository.countByRole(RoleEnum.ADMIN);
        if (userCount != 0 && adminCount != 0) {
            return mapper.toSimpleDto(repository.save(person));
        } else if (userCount == 0 && person.getRole() == RoleEnum.ADMIN) {
            return mapper.toSimpleDto(repository.save(person));
        } else {
            throw new IllegalArgumentException("First user must be declared as admin.");
        }
    }

    public PersonSimpleDTO getPerson(Long id) {
        return mapper.toSimpleDto(repository.findById(id).orElseThrow());
    }

    public Optional<Person> getPersonByUsername(String username) {
        return repository.findByUsername(username);
    }

    public String deletePersonByUsername(String username) 
    {
        long affectedRows = repository.deleteByUsername(username);
        
        if (affectedRows >= 1) 
        {
            return "Person with username '" + username + "' deleted successfully.";
        } 
        else 
        {
            return "Person with username '" + username + "' not found.";
        }
    }
    
    public Optional<Person> getPersonByTag(String personalTag) {
        return repository.findByTag(personalTag);
    }
}
