/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package hig.service.payment;

import hig.domain.Payment;
import java.util.List;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

/**
 *
 * @author 21axed01
 */
@Component
@Mapper(componentModel = "spring")
public interface PaymentMapper {
    
    PaymentDTO toDTO(Payment payment);
    
    List<PaymentDTO> toDtoList(List<Payment> payments);
}
