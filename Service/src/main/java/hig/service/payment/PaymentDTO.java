/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hig.service.payment;

import hig.service.person.PersonDTO;
import java.time.LocalDate;

/**
 *
 * @author 21axed01
 */
public record PaymentDTO(PersonDTO person, LocalDate date) {
    
}
