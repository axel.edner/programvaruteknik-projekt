/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hig.service.payment;

import hig.domain.Payment;
import hig.domain.Person;
import hig.repository.PaymentRepository;
import hig.repository.PersonRepository;
import hig.service.date.DateProviderAdapter;
import hig.service.person.PersonDTO;
import jakarta.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author 21axed01
 */
@Service
public class PaymentService {
    
    private final PaymentRepository repository;
    private final PersonRepository personRepository;
    private final PaymentMapper mapper;
    private final DateProviderAdapter adapter;
    
    @Autowired
    public PaymentService(PaymentRepository repository, PersonRepository personRepository, PaymentMapper mapper, DateProviderAdapter adapter) {
        this.repository = repository;
        this.personRepository = personRepository;
        this.mapper = mapper;
        this.adapter = adapter;
    }

    public PaymentDTO create(Long personId, float pay) {
        Person p = personRepository.findById(personId).orElseThrow(() -> new EntityNotFoundException("No person with given id found"));
        return mapper.toDTO(repository.save(new Payment(p, pay)));
    }
    
    public PaymentDTO create(Long personId, float pay, String dateString) {
        Person person = personRepository.findById(personId).orElseThrow(() -> new EntityNotFoundException("No person with given id found"));
        LocalDate inputDate;
        LocalDate currentDate = adapter.getCurrentDate();
        LocalDate previousMonthPayDayDate = adapter.getPreviousMonthPayDayDate();
        LocalDate currentMonthPayDayDate = adapter.getCurrentMonthPayDayDate();
        try {
            inputDate = adapter.parse(dateString);
        } catch (DateTimeParseException ex) {
            throw new IllegalArgumentException("Invalid date format");
        }
        
        if (inputDate.isBefore(previousMonthPayDayDate.plusDays(1)) || currentDate.isAfter(currentMonthPayDayDate) && inputDate.isBefore(currentMonthPayDayDate.plusDays(1))) {
            throw new IllegalArgumentException("Invalid date.");
        }
        else {
            return mapper.toDTO(repository.save(new Payment(person, pay, inputDate)));
        }
    }

}
