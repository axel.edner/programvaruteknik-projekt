package hig.service.logging;

import java.util.function.Supplier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 *
 * @author thomas
 */
@Component
@PropertySource("classpath:application.properties")
public class FilterableLogAdapter implements LogAdapter {

    @Value("${logging:true}")
    private boolean enabled;
    private final LogAdapter delegate = new Log4jAdapter();
    
    @Override
    public void log(Level level, Supplier<String> messageSupplier) {
        if (enabled) {
            delegate.log(level, messageSupplier);
        }
    }
}
