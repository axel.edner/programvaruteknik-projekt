package hig.service.logging;

import java.util.Map;
import java.util.function.*;
import org.apache.logging.log4j.*;

/**
 *
 * @author thomas
 */
public class Log4jAdapter implements LogAdapter {

    private static final Logger logger = LogManager.getLogger("LOG4J");
    private final Map<Level, Consumer<String>> methodMap
            = Map.of(Level.INFO, logger::info,
                    Level.DEBUG, logger::debug,
                    Level.WARNING, logger::warn,
                    Level.ERROR, logger::error);

    @Override
    public void log(Level level, Supplier<String> messageSupplier) {
        methodMap.get(level).accept(messageSupplier.get());
    }
}
