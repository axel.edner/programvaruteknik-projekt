package hig.service.cleaning;

import hig.domain.Cleaning;
import java.util.List;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

/**
 *
 * @author thomas
 */
@Component
@Mapper(componentModel = "spring")
public interface CleaningMapper {

    CleaningDTO toDto(Cleaning cleaning);
    
    List<CleaningDTO> toDtoList(List<Cleaning> cleanings);
}
