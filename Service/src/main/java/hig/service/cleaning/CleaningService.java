package hig.service.cleaning;

import hig.domain.*;
import hig.repository.*;
import jakarta.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author thomas
 */
@Service
public class CleaningService {

    private final CleaningRepository repository;
    private final PersonRepository personRepository;
    private final RoomRepository roomRepository;
    private final CleaningMapper mapper;

    @Autowired
    public CleaningService(CleaningRepository repository, PersonRepository personRepository, RoomRepository roomRepository, CleaningMapper mapper) {
        this.repository = repository;
        this.personRepository = personRepository;
        this.roomRepository = roomRepository;
        this.mapper = mapper;
    }

    public List<CleaningDTO> getByRoomIdAndDaysOpenForward(Long roomId, Long days) {
        return mapper.toDtoList(repository.findByWhereIdAndWhenGreaterThan(roomId, LocalDateTime.now().minusDays(days)));
    }
    
    public List<CleaningDTO> getByRoomIdForCurrentDay(Long roomId) {
        return mapper.toDtoList(repository.findByWhereIdAndWhenGreaterThan(roomId, LocalDateTime.now().with(LocalTime.of(0, 0))));
    }
    
    public List<CleaningDTO> getByUserIdAndDaysOpenForward(Long userId, Long days) {
        return mapper.toDtoList(repository.findByWhoIdAndWhenGreaterThan(userId, LocalDateTime.now().minusDays(days)));
    }
    
    public List<CleaningDTO> getByUserIdAndDaysOpenBackward(Long userId, Long days) {
        return mapper.toDtoList(repository.findByWhoIdAndWhenLessThan(userId, LocalDateTime.now().minusDays(days)));
    }
    
    public List<CleaningDTO> getByUserIdAndTimeRange(Long userId, Long daysFrom, Long daysTo) {
        return mapper.toDtoList(repository.findByWhoIdAndWhenBetween(userId, LocalDateTime.now().minusDays(daysFrom), LocalDateTime.now().minusDays(daysTo)));
    }
    
    public CleaningDTO create(Long roomId, String personalKey) {
        Person p = personRepository.findByTag(personalKey).orElseThrow(() -> new EntityNotFoundException("No person with given key found"));
        Room r = roomRepository.findById(roomId).orElseThrow(() -> new EntityNotFoundException("No room with given id found"));
        return mapper.toDto(repository.save(new Cleaning(p, r)));
    }
}
