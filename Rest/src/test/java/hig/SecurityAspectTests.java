/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package hig;

import hig.aspect.SecurityAspect;
import hig.domain.Person;
import hig.domain.RoleEnum;
import hig.repository.PersonRepository;
import hig.service.person.PersonService;
import jakarta.servlet.http.HttpServletRequest;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author 22gupe01
 */
@ExtendWith(MockitoExtension.class)
public class SecurityAspectTests {

    @Mock
    PersonRepository mockRepository;
    @Mock
    PersonService mockService;
    @Mock
    HttpServletRequest mockRequest;

    @InjectMocks
    SecurityAspect objectUnderTest = new SecurityAspect();

    private final String firstName = "Axel";
    private final String lastName = "Persson";
    private final String username = "Test";
    private final String password = "123";
    private final String faultyPersonalTag = UUID.randomUUID().toString();
    private final String wrongPassword = "?";
    private final String wrongUsername = "?";
    private final int age = 55;
    private RoleEnum role = RoleEnum.ADMIN;
    private Person testPerson;

    public SecurityAspectTests() {
    }

    @BeforeAll
    public static void setUpClass() {

    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
        testPerson = new Person(firstName, lastName, age, username, password, role);

    }

    @AfterEach
    public void tearDown() {
        testPerson = null;
    }

    @Test
    public void firstRequestTest() {
        Mockito.when(mockRepository.count()).thenReturn((long) 0);
        assertDoesNotThrow(() -> objectUnderTest.checkAuthentication());
    }

    @Test
    public void testLoginSuccessWithExistingUsers() {
        Mockito.when(mockRepository.count()).thenReturn((long) 2);
        Mockito.when(mockRequest.getHeader("personalTag")).thenReturn(null);
        Mockito.when(mockRequest.getHeader("username")).thenReturn(testPerson.getUsername());
        Mockito.when(mockService.getPersonByUsername(username)).thenReturn(Optional.of(testPerson));
        Mockito.when(mockRequest.getHeader("password")).thenReturn(testPerson.getPassword());

        assertDoesNotThrow(() -> objectUnderTest.checkAuthentication());
    }

    @Test
    public void invalidUserNameTest() {
        Mockito.when(mockRepository.count()).thenReturn((long) 2);
        Mockito.when(mockRequest.getHeader("personalTag")).thenReturn(null);
        Mockito.when(mockRequest.getHeader("username")).thenReturn(wrongUsername);
        Mockito.when(mockService.getPersonByUsername(wrongUsername)).thenReturn(Optional.empty());

        try {
            objectUnderTest.checkAuthentication();
            fail("No exception was thrown where a SecurityException is expected!");
        } catch (SecurityException e) {
            assertEquals("Invalid user", e.getMessage());
        } catch (Exception e) {
            fail("An unexpected exception was thrown where only SecurityException is expected!");
        }
    }

    @Test
    public void invalidPassWordTest() {
        Mockito.when(mockRepository.count()).thenReturn((long) 2);
        Mockito.when(mockRequest.getHeader("personalTag")).thenReturn(null);
        Mockito.when(mockRequest.getHeader("username")).thenReturn(testPerson.getUsername());
        Mockito.when(mockService.getPersonByUsername(username)).thenReturn(Optional.of(testPerson));
        Mockito.when(mockRequest.getHeader("password")).thenReturn(wrongPassword);

        try {
            objectUnderTest.checkAuthentication();

            fail("No exception was thrown where a SecurityException is expected!");
        } catch (SecurityException e) {
            assertEquals("Invalid password", e.getMessage());
        } catch (Exception e) {
            fail("An unexpected exception was thrown where only SecurityException is expected!");
        }
    }

    @Test
    public void successfulAuthorizationTest() {
        Mockito.when(mockRequest.getHeader("username")).thenReturn(testPerson.getUsername());
        Mockito.when(mockService.getPersonByUsername(username)).thenReturn(Optional.of(testPerson));

        assertDoesNotThrow(() -> objectUnderTest.checkAuthorization());
    }

    @Test
    public void unSuccessfulAuthorizationTest() {
        testPerson.setRole(RoleEnum.USER);
        Mockito.when(mockRequest.getHeader("username")).thenReturn(testPerson.getUsername());
        Mockito.when(mockService.getPersonByUsername(username)).thenReturn(Optional.of(testPerson));

        try {
            objectUnderTest.checkAuthorization();
            fail("A SecurityException should have been thrown!");
        } catch (SecurityException e) {
            assertEquals("Request not authorized.", e.getMessage());
        } catch (Exception e) {
            fail("An unexpected exception was thrown where only SecurityException is expected!");
        }
    }
    
    @Test
    public void successfulAuthenticationWithPersonalTagTest() {
        Mockito.when(mockRepository.count()).thenReturn(1L);
        Mockito.when(mockRequest.getHeader("personalTag")).thenReturn(testPerson.getTag());
        Mockito.when(mockRequest.getHeader("personalTag")).thenReturn(testPerson.getTag());
        Mockito.when(mockService.getPersonByTag(testPerson.getTag())).thenReturn(Optional.of(testPerson));
        assertDoesNotThrow(() -> objectUnderTest.checkAuthentication());
    }
    
    @Test
    public void unsuccessfulAuthenticationWithPersonalTagTest() {
        Mockito.when(mockRepository.count()).thenReturn(1L);
        Mockito.when(mockRequest.getHeader("personalTag")).thenReturn(faultyPersonalTag);
        Mockito.when(mockRequest.getHeader("personalTag")).thenReturn(faultyPersonalTag);
        Mockito.when(mockService.getPersonByTag(faultyPersonalTag)).thenReturn(Optional.empty());
        assertThrows(SecurityException.class,() -> objectUnderTest.checkAuthentication());
    }
    
    @Test
    public void successfulCheckGetPermissionsTest() {
        testPerson.setRole(RoleEnum.USER);
        Mockito.when(mockRequest.getHeader("username")).thenReturn(testPerson.getUsername());
        Mockito.when(mockService.getPersonByUsername(testPerson.getUsername())).thenReturn(Optional.ofNullable(testPerson));
        Mockito.when(mockRequest.getHeader("requestedUser")).thenReturn(testPerson.getUsername());
        assertDoesNotThrow(() -> objectUnderTest.checkGetPermissions());
    }
    
    @Test
    public void unsuccessfulCheckGetPermissionsTest() {
        testPerson.setRole(RoleEnum.USER);
        Mockito.when(mockRequest.getHeader("username")).thenReturn(testPerson.getUsername());
        Mockito.when(mockService.getPersonByUsername(testPerson.getUsername())).thenReturn(Optional.ofNullable(testPerson));
        Mockito.when(mockRequest.getHeader("requestedUser")).thenReturn("otherUser");
        assertThrows(SecurityException.class,() -> objectUnderTest.checkGetPermissions());
    }
    
    @Test
    public void checkGetPermissionsAsAdminTest() {
        testPerson.setRole(RoleEnum.ADMIN);
        Mockito.when(mockRequest.getHeader("username")).thenReturn(testPerson.getUsername());
        Mockito.when(mockService.getPersonByUsername(testPerson.getUsername())).thenReturn(Optional.ofNullable(testPerson));
        assertDoesNotThrow(() -> objectUnderTest.checkGetPermissions());
    }

}
