/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package hig.rest;

import hig.BasicApplication;
import hig.domain.Person;
import hig.domain.RoleEnum;
import jakarta.transaction.Transactional;
import org.json.JSONException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
//import java.net.http.HttpHeaders;

/**
 *
 * @author 22gupe01
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = BasicApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Transactional
public class PersonControllerIT {

    @LocalServerPort
    private int port;

    private final String username = "test";
    private final String password = "test";
    private final String headerUsername = "username";
    private final String headerPassword = "password";
    private final String headerContent = "Content-Type";
    private final String contentValue = "application/json";
    private final String firstName = "Göran";
    private final String lastName = "Bengtsson";
    private final Integer age = 10;
    private final String username2 = "gb10";
    private final String password2 ="gb10";

    Person testPerson;
    
    TestRestTemplate restTemplate = new TestRestTemplate();

    HttpHeaders headers;

    HttpEntity<String> getEntity;
    HttpEntity<Person> postEntity;
    HttpEntity<Person> deleteEntity;

    ResponseEntity<String> response;
    private String expected;

    public PersonControllerIT() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
        headers = new HttpHeaders();
        headers.add(headerUsername, username);
        headers.add(headerPassword, password);
        headers.add(headerContent, contentValue);
        testPerson = new Person(firstName, lastName, age, username2, password2, RoleEnum.ADMIN);

    }

    @AfterEach
    public void tearDown() {
        headers = null;
        getEntity = null;
        postEntity = null;
        response = null;
        testPerson = null;
    }

    @Test
    public void testFindOne() throws JSONException {

        getEntity = new HttpEntity<>(headers);
        response = restTemplate.exchange(createURLWithPort("/person/1"),
                HttpMethod.GET, getEntity, String.class);

        expected = "{\"name\":\"ADMIN ADMINATOR\",\"age\": 9999}";

        JSONAssert.assertEquals(expected, response.getBody(), false);
        System.out.println(response.getBody());

    }
    
    @Test
    public void testCreatePerson() {
        postEntity = new HttpEntity<>(testPerson, headers);

        response = restTemplate.exchange(createURLWithPort("/person"), HttpMethod.POST, postEntity, String.class);

        expected = "{\"name\":\"" + firstName + " " + lastName + "\",\"age\":" + age + "}";
        
        assertEquals(expected, response.getBody());
        System.out.println(response.getBody());

    }
    @Test
    public void testDeletePerson() {
        deleteEntity = new HttpEntity<>(testPerson, headers);
        
        response = restTemplate.exchange(createURLWithPort("/person/" + testPerson.getUsername()), HttpMethod.DELETE, deleteEntity, String.class);
        
        expected = "Person with username '" + testPerson.getUsername() + "' deleted successfully.";
        
        assertEquals(expected, response.getBody());
        
        System.out.println(response.getBody());
    }
    
    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }

}
