/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package hig.rest;

import hig.domain.Person;
import hig.service.person.PersonService;
import hig.service.person.PersonSimpleDTO;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author Ole Granberg
 */
@ExtendWith(MockitoExtension.class)
public class PersonControllerTest {
    
    @Mock
    PersonService mockService;
    
    @InjectMocks
    PersonController objectUndertest;
    
    public PersonControllerTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of findAll method, of class PersonController.
     */
    @Test
    public void testFindAll() {
        System.out.println("findAll");

    }

    /**
     * Test of findOne method, of class PersonController.
     */
    @Test
    public void testFindOne() {
        System.out.println("findOne");

    }

    /**
     * Test of createNew method, of class PersonController.
     */
    @Test
    public void testCreateNew() {
        System.out.println("createNew");

    }

    /**
     * Test of delete method, of class PersonController.
     */
    @Test
    public void testSuccessfulDelete() {
        System.out.println("Test of successful delete");
        
        String username = "Test";
        String yes = "Person with username Anders has been deleted.";
        
        Mockito.when(mockService.deletePersonByUsername(username)).thenReturn(yes);
        
        String result = objectUndertest.delete(username);
        
        assertEquals(result, yes);
    }
    
    /**
     * Test of delete method, of class PersonController.
     */
    @Test
    public void testUnsuccessfulDelete() {
        System.out.println("Test of unsuccessful delete");
        
        String no = "Person with username Anders could not be found.";
        String username = "Test";
        
        Mockito.when(mockService.deletePersonByUsername(username)).thenReturn(no);
        
        String result = objectUndertest.delete(username);
        
        assertEquals(result, no);
    }
    
}
