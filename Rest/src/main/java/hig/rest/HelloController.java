package hig.rest;

import org.springframework.web.bind.annotation.*;

/**
 *
 * @author thomas
 */
@RestController
@RequestMapping("hello")
public class HelloController {

    @GetMapping
    public String sayHello() {
        return "Hello, world!";
    }

    @GetMapping(path = "{name}")
    public String sayHelloToCaller(@PathVariable String name) {
        return "Hello, " + name + "!";
    }

    @GetMapping(path = "abuse")
    public Message sayHelloToCaller() {
        return new Message("Hello, moron!");
    }
}
