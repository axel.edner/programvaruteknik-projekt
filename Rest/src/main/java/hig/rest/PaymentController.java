/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hig.rest;

import hig.domain.Payment;
import hig.service.payment.PaymentDTO;
import hig.service.payment.PaymentService;
import hig.service.person.PersonService;
import java.time.LocalDateTime;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author 21axed01
 */
@RestController
@RequestMapping("pay")
public class PaymentController {
    
    private final PaymentService service;
    private final PersonService personService;
    
    public PaymentController(PaymentService service, PersonService personService) {
        this.service = service;
        this.personService = personService;
    }
    
    @PostMapping("{personId}/{pay}")
    public PaymentDTO create(@PathVariable Long personId, @PathVariable float pay) {
        return service.create(personId, pay);
    }
    
    @PostMapping("{personId}/{pay}/{date}")
    public PaymentDTO create(@PathVariable Long personId, @PathVariable float pay, @PathVariable String date) {
        return service.create(personId, pay, date);
    }
    
}
