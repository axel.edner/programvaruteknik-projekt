package hig.rest;

import hig.domain.Room;
import hig.service.room.*;
import java.util.List;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author Ruskiga refaktorerarna
 */
@RestController
@RequestMapping("room")
public class RoomController {

    private final RoomService service;
    private final RoomMapper mapper;

    public RoomController(
            RoomService service, 
            RoomMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @PostMapping
    public RoomDTO create(@RequestBody Room room) {
        return mapper.toDto(service.create(room));
    }
    
    @GetMapping
    public List<RoomDTO> findAll() {
        return service.findAll();
    }
    
    @DeleteMapping("{id}")
    public String delete(@PathVariable Long id) {
        return service.delete(id);
        
    }
}
