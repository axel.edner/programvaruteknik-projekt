
package hig.rest;

import hig.service.person.PersonService;
import hig.domain.Person;
import hig.service.person.*;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author thomas
 */
@RestController
@RequestMapping("person")
public class PersonController {

    private final PersonService service;

    @Autowired
    public PersonController(PersonService service) {
        this.service = service;
    }

    @GetMapping
    public List<PersonSimpleDTO> findAll() {
        return service.getAllPeople();
    }

    @GetMapping("{id}")
    public PersonSimpleDTO findOne(@PathVariable Long id) {
        return service.getPerson(id);
    }

    @PostMapping
    public PersonSimpleDTO create(@RequestBody Person person) {
        return service.create(person);
    }
    
    @DeleteMapping("{username}")
    public String delete(@PathVariable String username) 
    {
        return service.deletePersonByUsername(username);
    }
}
