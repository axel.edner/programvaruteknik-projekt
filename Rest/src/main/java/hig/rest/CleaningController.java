package hig.rest;

import hig.service.cleaning.*;
import hig.service.person.PersonService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author thomas
 */
@RestController
@RequestMapping("cleaning")
public class CleaningController {

    private final CleaningService service;
    private final PersonService personService;

    @Autowired
    public CleaningController(CleaningService service, PersonService personService) {
        this.service = service;
        this.personService = personService;
    }

    @PostMapping("{roomId}")
    public CleaningDTO create(@RequestHeader String personalTag, @PathVariable Long roomId) {
        return service.create(roomId, personalTag);
    }

    @GetMapping("{roomId}/{days}")
    public List<CleaningDTO> getByRoomIdAndDaysOpenForward(@PathVariable Long roomId, @PathVariable Long days) {
        return service.getByRoomIdAndDaysOpenForward(roomId, days);
    }

    @GetMapping("{roomId}")
    public List<CleaningDTO> getByRoomIdForCurrentDay(@PathVariable Long roomId) {
        return service.getByRoomIdForCurrentDay(roomId);
    }

    @GetMapping("{days}/f")
    public List<CleaningDTO> getByUserAndDaysOpenForward(@RequestHeader("requestedUser") String username, @PathVariable Long days) {
        return service.getByUserIdAndDaysOpenForward(personService.getPersonByUsername(username).orElseThrow().getId(), days);
    }

    @GetMapping("{days}/b")
    public List<CleaningDTO> getByUserAndDaysOpenBackward(@RequestHeader("requestedUser") String username, @PathVariable Long days) {
        return service.getByUserIdAndDaysOpenBackward(personService.getPersonByUsername(username).orElseThrow().getId(), days);
    }

    @GetMapping("{daysFrom}/{daysTo}/t")
    public List<CleaningDTO> getByUserAndTimeRange(@RequestHeader("requestedUser") String username, @PathVariable Long daysFrom, @PathVariable Long daysTo) {
        return service.getByUserIdAndTimeRange(personService.getPersonByUsername(username).orElseThrow().getId(), daysFrom, daysTo);
    }

}
