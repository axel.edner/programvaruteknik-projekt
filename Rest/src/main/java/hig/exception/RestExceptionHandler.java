package hig.exception;

import jakarta.validation.ConstraintViolationException;
import java.util.stream.Collectors;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 *
 * @author thomas
 */
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {SecurityException.class})
    public ResponseEntity<ExceptionWrapper> handleSecurityException(SecurityException ex) {
        return new ResponseEntity<>(new ExceptionWrapper(ex), HttpStatus.FORBIDDEN);
    }
    
    @ExceptionHandler(value = {IllegalArgumentException.class})
    public ResponseEntity<ExceptionWrapper> handleIllegalArgumentException(IllegalArgumentException ex) {
        return new ResponseEntity<>(new ExceptionWrapper(ex), HttpStatus.NOT_ACCEPTABLE);
    }
    
    @ExceptionHandler(value = {ConstraintViolationException.class})
    public ResponseEntity<ExceptionWrapper> handleConstraintViolationException(ConstraintViolationException ex) {
        String message = ex.getConstraintViolations().stream()
                .map(violation -> violation.getMessage())
                .collect(Collectors.joining(", "));
        return new ResponseEntity<>(new ExceptionWrapper(new IllegalArgumentException(message)), HttpStatus.NOT_ACCEPTABLE);
    }

    public static class ExceptionWrapper {

        private final String type;
        private final String message;

        public ExceptionWrapper(Exception ex) {
            this.type = ex.getClass().getSimpleName();
            this.message = ex.getMessage();
        }

        public String getType() {
            return type;
        }

        public String getMessage() {
            return message;
        }
    }
}
