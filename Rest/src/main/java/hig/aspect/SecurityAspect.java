package hig.aspect;

import hig.domain.Person;
import hig.domain.RoleEnum;
import hig.repository.PersonRepository;
import hig.service.person.PersonService;
import jakarta.servlet.http.HttpServletRequest;
import java.util.Optional;
import org.aspectj.lang.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 *
 * @author thomas
 */
@Aspect
@Component
public class SecurityAspect {

    String personalTagString = "personalTag";
    String usernameString = "username";
    String passwordString = "password";

    @Autowired
    private HttpServletRequest request;
    @Autowired
    private PersonService personService;
    @Autowired
    private PersonRepository personRepository;

    @Pointcut("execution(* hig.rest.*Controller.create(..))")
    public void createNew() {
    }

    @Pointcut("execution(* hig.rest.*Controller.delete(..))")
    public void delete() {
    }

    @Pointcut("execution(* hig.rest.CleaningController.getByUserAndDaysOpenForward(..))")
    public void getByUserAndDaysOpenForward() {
    }

    @Pointcut("execution(* hig.rest.CleaningController.getByUserAndDaysOpenBackward(..))")
    public void getByUserAndDaysOpenBackward() {
    }

    @Pointcut("execution(* hig.rest.CleaningController.getByUserAndTimeRange(..))")
    public void getByUserAndTimeRange() {
    }

    @Before("getByUserAndDaysOpenForward() || getByUserAndDaysOpenBackward() || getByUserAndTimeRange()")
    public void checkGetPermissions() {
        String username = Optional.ofNullable(request.getHeader(usernameString)).orElseThrow();
        Person user = personService.getPersonByUsername(username).orElseThrow(() -> new SecurityException("Invalid username or password"));
        if (user.getRole() != RoleEnum.ADMIN) {
            String requestedUserString = Optional.ofNullable(request.getHeader("requestedUser")).orElseThrow();
            if (!requestedUserString.equals(username)) {
                throw new SecurityException("Request not authorized.");
            }
        }
    }

    @Before("execution(* hig.rest..*Controller.*(..)))")
    public void checkAuthentication() {
        long userCount = personRepository.count();
        if (userCount != 0) {
            if (Optional.ofNullable(request.getHeader(personalTagString)).isPresent()) {
                String tag = Optional.ofNullable(request.getHeader(personalTagString)).orElseThrow();
                personService.getPersonByTag(tag).orElseThrow(() -> new SecurityException("No user with given key found"));
            } else {
                String username = Optional.ofNullable(request.getHeader(usernameString)).orElseThrow();
                Person user = personService.getPersonByUsername(username).orElseThrow(() -> new SecurityException("Invalid user"));
                Optional.ofNullable(request.getHeader(passwordString))
                        .filter(p -> p.equals(user.getPassword()))
                        .orElseThrow(() -> new SecurityException("Invalid password"));
            }
        }
    }

    @Before("createNew() || delete()")
    public void checkAuthorization() {
        String username = Optional.ofNullable(request.getHeader(usernameString)).orElseThrow();
        Person user = personService.getPersonByUsername(username).orElseThrow(() -> new SecurityException("Invalid username or password"));
        if (user.getRole() != RoleEnum.ADMIN) {
            throw new SecurityException("Request not authorized.");
        }
    }
}
