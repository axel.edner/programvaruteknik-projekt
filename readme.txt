Viktiga länkar:

Spring boot User and Roles:

https://medium.com/@bubu.tripathy/role-based-access-control-with-spring-security-ca59d2ce80b0
https://blog.tericcabrel.com/role-base-access-control-spring-boot/
https://www.baeldung.com/role-and-privilege-for-spring-security-registration

För att förstå innehållet i SecurityAspect.java bättre så lät jag ChatGPT sammanfatta information från två hemsidor:

"Aspect-Oriented Programming (AOP) is a programming paradigm that allows you to separate cross-cutting concerns from the main business logic of your application. In simpler terms, imagine you have a piece of code that needs to be applied across multiple parts of your program, like logging or security checks. AOP helps you extract these common functionalities into separate modules called aspects.

An aspect, in the context of AOP, is a modular unit that encapsulates a cross-cutting concern. These concerns are functionalities that affect multiple parts of your program but are conceptually separate from the main logic. Aspects can include tasks like logging, security checks, transaction management, and error handling. By encapsulating these concerns into aspects, you can modularize and manage them separately from the core functionalities of your application."

source: https://naveen-metta.medium.com/deep-dive-into-aspect-oriented-programming-aop-in-spring-and-spring-boot-afcb29141cbd
source: https://www.geeksforgeeks.org/spring-aop-aspectj-annotation/
