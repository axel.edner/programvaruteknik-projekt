package hig.repository;

import hig.domain.Cleaning;
import java.time.*;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author thomas
 */
@Repository
public interface CleaningRepository extends JpaRepository<Cleaning, Long>{

    public List<Cleaning> findByWhereIdAndWhenGreaterThan(Long roomId, LocalDateTime from);
    
    public List<Cleaning> findByWhoIdAndWhenGreaterThan(Long userId, LocalDateTime from);
    
    public List<Cleaning> findByWhoIdAndWhenLessThan(Long userId, LocalDateTime from);
    
    public List<Cleaning> findByWhoIdAndWhenBetween(Long userId, LocalDateTime from, LocalDateTime to);
    
    public long countByWhereId(long id);
}
