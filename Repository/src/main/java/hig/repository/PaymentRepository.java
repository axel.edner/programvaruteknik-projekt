/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package hig.repository;

import hig.domain.Payment;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author 21axed01
 */
public interface PaymentRepository extends JpaRepository<Payment, Long> {
    
    
}
