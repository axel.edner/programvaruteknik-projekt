package hig.repository;

import hig.domain.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author thomas
 */
@Repository
public interface RoomRepository extends JpaRepository<Room, Long> {
    
    
    @Transactional
    public Long deleteRoomById(Long id);
}
