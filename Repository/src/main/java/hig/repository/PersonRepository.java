package hig.repository;

import hig.domain.Person;
import hig.domain.RoleEnum;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author thomas
 */
@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {

    public Optional<Person> findByTag(String personalTag);
    
    public Optional<Person> findByUsername(String username);
    
    public long countByRole(RoleEnum role);
    
    @Transactional
    public long deleteByUsername(String username);
}
