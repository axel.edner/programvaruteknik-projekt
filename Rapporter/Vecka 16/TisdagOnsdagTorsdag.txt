Tisdag:

Vi höll på med CommandLineRunner/ApplicationRunner för att köra en check i databasen ifall det finns användare.

Detta skrotas i slutet av sessionen pga. upptäcks finnas enklare sätt att lösa denna problematik.

Onsdag:

Börjar med joinPoint/ProceedingJoinPoint i SecurityAspect för att hantera anrop vid skapning av den första Person i databasen. Tanken är först att injicera argument i anropet för att se till att första användare blir admin. Efter mycket strul med detta så snackar vi med Åke och inser att vi kan göra detta lättare genom att kasta exception.

Torsdag:

Efter snack med Thomas så inser vi att SecurityAspect inte ska hantera detta utan Service-lagret ska hantera logiken kring att kolla argument från anropet. SecurityAspect ska bara hantera själva checken om användaren får komma in.

Vi fixar till servicelagret så det hanterar detta och SecurityAspect hanterar nu bara authentication. Vi slänger genom PersonService ett exception om första användaren kommer in med fel argument.