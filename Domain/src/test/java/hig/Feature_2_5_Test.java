/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package hig;

import hig.domain.Person;
import hig.domain.RoleEnum;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author 22gupe01
 */
public class Feature_2_5_Test {
    
    static Person person;
    static String firstName = "test";
    static String lastName = "test";
    static int age = 66;
    static String username = "test";
    static String password = "test";
    static RoleEnum role;
    
    public Feature_2_5_Test() {
    }
    
    @BeforeAll
    public static void setUpClass() {
        role = role.USER;
        person = new Person(firstName, lastName, age, username, password, role);
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }
    
    /**
     * Test för feature_2
     */
    @Test
    public void testUserRoles() {
        person.setRole(RoleEnum.USER);
        assertEquals(RoleEnum.USER, person.getRole());
        person.setRole(RoleEnum.ADMIN);
        assertEquals(RoleEnum.ADMIN, person.getRole());
        assertNotEquals(RoleEnum.USER, person.getRole());
    }
    /**
     * Test för feature_5
     */
    @Test
    public void noRoleSpecifiedAutoAssignsUserTest() {
        
        Person noRolePerson = new Person(firstName, lastName, age, username, password);
        assertEquals(RoleEnum.USER, noRolePerson.getRole());
    }
    
}
