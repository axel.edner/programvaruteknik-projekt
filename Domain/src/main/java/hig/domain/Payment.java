/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hig.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import java.time.LocalDate;

/**
 *
 * @author 21axed01
 */
@Entity
public class Payment {
    
    public static final int PAY_DAY = 25;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @ManyToOne
    private Person person;
    
    @Column
    private float compensation;
    
    @Column
    private LocalDate date = LocalDate.now();
    
    public Payment() {
    }
    
    public Payment(Person person, float compensation) {
        this.person = person;
        this.compensation = compensation;
    }
    
    public Payment(Person person, float compensation, LocalDate date) {
        this.person = person;
        this.compensation = compensation;
        this.date = date;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public float getCompensation() {
        return compensation;
    }

    public void setCompensation(float compensation) {
        this.compensation = compensation;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
    
}
