package hig.domain;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;

/**
 *
 * @author thomas
 */
@Entity
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotEmpty(message = "Name must not be null or empty")
    private String name;
    
    protected Room() {
    }

    public Room(String name) {
        this.name = name;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
